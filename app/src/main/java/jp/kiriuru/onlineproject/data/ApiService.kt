package jp.kiriuru.onlineproject.data

import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("api/get")
    suspend fun searchQuotes(
        @Query("site") site: String,
        @Query("name") name: String,
        @Query("num") num: Int
    ): List<Quote>

    @GET("api/sources")
    suspend fun searchSources(): List<List<SourceOfQuotes>>

//    companion object Factory2 {
//        fun create(): ApiService {
//            val gson: Gson = GsonBuilder().setLenient().create()
//            val retrofit = Retrofit.Builder()
//                .addCallAdapterFactory(LiveDataCallAdapterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .baseUrl("http://umorili.herokuapp.com/")
//                .build()
//            return retrofit.create(ApiService::class.java)
//        }
//    }
}
