package jp.kiriuru.onlineproject.data

import com.google.gson.annotations.SerializedName

data class SourceOfQuotes(
    @SerializedName("site") val site: String,
    @SerializedName("name") val name: String,
    @SerializedName("url") val url: String,
    @SerializedName("parsel") val parsel: String,
    @SerializedName("encoding") val encoding: String,
    @SerializedName("linkpar") val linkPar: String,
    @SerializedName("desc") val desc: String,


    )