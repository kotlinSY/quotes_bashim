package jp.kiriuru.onlineproject.data

class SearchQuotesRepository(private val apiHelper: ApiHelper) {

    //    fun searchQuotes(site: String, name: String): io.reactivex.Observable<List<Quote>> {
//        return apiService.searchQuotes(site, name, 50)
//    }
//
//    fun searchSources(): io.reactivex.Observable<List<List<SourceOfQuotes>>> {
//        return apiService.searchSources()
//    }
    suspend fun searchQuotes(site: String, name: String) = apiHelper.searchQuotes(site, name)

    suspend fun searchSources() = apiHelper.searchSources()
//    suspend fun searchSources(): List<List<SourceOfQuotes>> {
//        return apiHelper.searchSources()
//    }
}