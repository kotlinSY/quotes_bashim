package jp.kiriuru.onlineproject.data

import com.google.gson.annotations.SerializedName

data class Quote(
    @SerializedName("site") val site: String,
    @SerializedName("desc") val desc: String,
    @SerializedName("name") val name: String,
    @SerializedName("link") val link: String,
    @SerializedName("elementPureHtml") val htmlText: String
)