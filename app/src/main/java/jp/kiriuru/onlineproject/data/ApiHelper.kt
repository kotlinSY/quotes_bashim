package jp.kiriuru.onlineproject.data

class ApiHelper(private val apiService: ApiService) {
    suspend fun searchQuotes(site: String, name: String) = apiService.searchQuotes(site, name, 50)

    suspend fun searchSources() = apiService.searchSources()

//    suspend fun searchSources(): List<List<SourceOfQuotes>> {
//        return apiService.searchSources()
//    }
}