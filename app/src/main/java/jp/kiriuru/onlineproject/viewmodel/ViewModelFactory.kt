package jp.kiriuru.onlineproject.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import jp.kiriuru.onlineproject.data.ApiHelper
import jp.kiriuru.onlineproject.data.SearchQuotesRepository

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(SearchQuotesRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}