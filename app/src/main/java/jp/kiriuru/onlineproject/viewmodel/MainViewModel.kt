package jp.kiriuru.onlineproject.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import jp.kiriuru.onlineproject.data.SearchQuotesRepository
import jp.kiriuru.onlineproject.utils.Resource
import kotlinx.coroutines.Dispatchers

class MainViewModel(private val repository: SearchQuotesRepository) : ViewModel() {
    fun searchSource() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.searchSources()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun searchQuotes(site: String, name: String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = repository.searchQuotes(site, name)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}