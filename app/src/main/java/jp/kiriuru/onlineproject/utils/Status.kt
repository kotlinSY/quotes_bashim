package jp.kiriuru.onlineproject.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}