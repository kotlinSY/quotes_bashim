package jp.kiriuru.onlineproject.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import jp.kiriuru.onlineproject.ChangeSourceListener
import jp.kiriuru.onlineproject.R
import jp.kiriuru.onlineproject.data.Quote
import kotlinx.android.synthetic.main.source_item.view.*

class QuotesAdapter(list: MutableList<Quote>) : RecyclerView.Adapter<QuotesAdapter.ViewHolder>() {


    private val mItems: MutableList<Quote> = list
    private val mListener: MutableList<ChangeSourceListener> = mutableListOf()


    override fun getItemCount(): Int = mItems.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.quotes_item, parent, false)
        return ViewHolder(view).listen { position, _ ->
            changeSource(position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: AppCompatTextView = view.text_card_view!!
    }

//    fun addListener(listener: ChangeSourceListener) {
//        mListener.add(listener)
//    }

    private fun changeSource(position: Int) {
        mListener.forEach {
            it.changedSource(position)
        }
    }

    private fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(absoluteAdapterPosition, itemViewType)
        }
        return this
    }

    fun addQuotes(quote: List<Quote>) {
        this.mItems.apply {
            clear()
            addAll(quote)
        }
    }

    operator fun get(position: Int): Quote {
        return mItems[position]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]
        holder.title.text = HtmlCompat.fromHtml(item.htmlText, HtmlCompat.FROM_HTML_MODE_LEGACY)
    }


}

