package jp.kiriuru.onlineproject

interface ChangeSourceListener {
    fun changedSource(position: Int)
}