package jp.kiriuru.onlineproject

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import jp.kiriuru.onlineproject.adapter.QuotesAdapter
import jp.kiriuru.onlineproject.data.ApiHelper
import jp.kiriuru.onlineproject.data.Quote
import jp.kiriuru.onlineproject.data.RetrofitBuilder
import jp.kiriuru.onlineproject.utils.Status
import jp.kiriuru.onlineproject.viewmodel.MainViewModel
import jp.kiriuru.onlineproject.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

const val INTENT_NAME_NAME = "name"
const val INTENT_SITE_NAME = "site"

class QuotesActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: QuotesAdapter

    private var list = mutableListOf<Quote>()
    private var mSite = ""
    private var mName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getExtra()
        setupViewModel()
        setupRecycle()
        setupObservers(mSite, mName)
    }


    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }

    private fun setupRecycle() {
        recycle_view.layoutManager = LinearLayoutManager(this)
        adapter = QuotesAdapter(arrayListOf())
        recycle_view.addItemDecoration(
            DividerItemDecoration(
                recycle_view.context,
                (recycle_view.layoutManager as LinearLayoutManager).orientation
            )
        )
        recycle_view.adapter = adapter

    }

    private fun setupObservers(site: String, name: String) {
        viewModel.searchQuotes(site, name).observe(this, {
            it?.let { resources ->
                when (resources.status) {
                    Status.SUCCESS -> {
                        resources.data?.let { result ->
                            list.addAll(result)
                        }
                        updateList(list)
                        Log.d(TAG, list.toString())
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                        Log.d(TAG, "${it.message}")
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
    }

    private fun getExtra() {
        mSite = intent.getStringExtra(INTENT_SITE_NAME).toString()
        mName = intent.getStringExtra(INTENT_NAME_NAME).toString()
    }

    private fun updateList(quote: List<Quote>) {
        adapter.apply {
            addQuotes(quote)
            notifyDataSetChanged()
        }
    }
}
