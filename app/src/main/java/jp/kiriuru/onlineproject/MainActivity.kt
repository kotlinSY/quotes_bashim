package jp.kiriuru.onlineproject

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import jp.kiriuru.onlineproject.adapter.SourceOfQuotesAdapter
import jp.kiriuru.onlineproject.data.ApiHelper
import jp.kiriuru.onlineproject.data.RetrofitBuilder
import jp.kiriuru.onlineproject.data.SourceOfQuotes
import jp.kiriuru.onlineproject.utils.Status
import jp.kiriuru.onlineproject.viewmodel.MainViewModel
import jp.kiriuru.onlineproject.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*

const val TAG: String = "MainActivity"

class MainActivity : AppCompatActivity(), ChangeSourceListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var adapter: SourceOfQuotesAdapter


    private var list = mutableListOf<SourceOfQuotes>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModel()
        setupRecycle()
        setupObservers()
    }


    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(ApiHelper(RetrofitBuilder.apiService))
        ).get(MainViewModel::class.java)
    }

    private fun setupRecycle() {
        recycle_view.layoutManager = LinearLayoutManager(this)
        adapter = SourceOfQuotesAdapter(arrayListOf())
        recycle_view.addItemDecoration(
            DividerItemDecoration(
                recycle_view.context,
                (recycle_view.layoutManager as LinearLayoutManager).orientation
            )
        )
        adapter.addListener(this)
        recycle_view.adapter = adapter

    }


    private fun setupObservers() {
        viewModel.searchSource().observe(this, {
            it?.let { resources ->
                when (resources.status) {
                    Status.SUCCESS -> {
                        resources.data?.let { result ->
                            result.forEach { i ->
                                list.addAll(i)
                            }
                            updateList(list)
                            Log.d(TAG, list.toString())
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                        Log.d(TAG, "${it.message}")
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })
    }

    private fun updateList(source: List<SourceOfQuotes>) {
        adapter.apply {
            addSource(source)
            notifyDataSetChanged()
        }
    }

    override fun changedSource(position: Int) {
        Log.d(TAG, "from ${adapter[position]}")
        val intent = Intent(applicationContext, QuotesActivity::class.java)
        intent.putExtra(INTENT_NAME_NAME, adapter[position].name)
        intent.putExtra(INTENT_SITE_NAME, adapter[position].site)
        startActivity(intent)
    }
}